/* eslint-disable no-underscore-dangle */
/* eslint-disable no-use-before-define */
/* eslint-disable no-undef */
const watch = require('node-watch');

const fs = require('fs');
const dotenv = require('dotenv').config({ path: './.env', });

var file;

fs.readFile('./.vscode/settings.json', 'utf8', function (err, data) {
  if (err) throw err;
  try {
    // call dotenv and it will return an Object with a parsed key
    const env = dotenv.parsed;

    // reduce it to a nice object, the same as before
    const envKeys = Object.keys(env).reduce((prev, next) => {
      // prev[`process.env.${next}`] = JSON.stringify(env[next]);
      prev[`${next}`] = env[next];
      return prev;
    }, {});

    file = JSON.parse(data);

    file.env = envKeys;

    fs.writeFile('./src/config.json', JSON.stringify(file.env, null, 2),
      function (err) {
        if (err) return console.log(err);
        // console.log(JSON.stringify(file, null, 2));
        console.log('writing to config.json');
      });
    fs.writeFile('./.vscode/settings.json', JSON.stringify(file, null, 2),
      function (err) {
        if (err) return console.log(err);
        // console.log(JSON.stringify(file, null, 2));
        console.log('writing to settings.json');
      });
  } catch (e) {
    console.error(e);
  }
});
