/* eslint-disable max-len */
/**
 * ? What to do when the file in the sharepoint is newer than my file?
 * TODO: check out if the file is not checked out.
 *
 */
const watch = require('node-watch');
const puppeteer = require('puppeteer-core');


const dotenv = require('dotenv').config({ path: './.env' });

// it will only work if we are on a list page. This makes sure we have all the dependencies available.
console.log(process.env.PUPPETEER_PROFILE_PATH);
(async () => {
  const browser = await puppeteer.launch({ headless: false, executablePath: process.env.CHROME_PATH, userDataDir: process.env.PUPPETEER_BROWSER_DATA });
  const page = await browser.newPage();
  await page.goto(process.env.DEV_SITE_URL + process.env.PUPPETEER_WATCH_PATH);
  // it will only work if we are on a list page. This makes sure we have all the dependencies available.
  await page.addScriptTag({url: 'https://code.jquery.com/jquery-3.2.1.min.js'})
  await page.waitForSelector('#spOnePageContext');
  await page.evaluate(() => {
    var s = document.createElement('input');
    s.type = 'file';
    s.id = 'getFile';
    // s.src = src;
    var t = document.getElementsByTagName('body')[0];
    t.parentNode.insertBefore(s, t);
  });
  const env = dotenv.parsed;

  // reduce it to a nice object, the same as before
  const envKeys = Object.keys(env).reduce((prev, next) => {
    // prev[`process.env.${next}`] = JSON.stringify(env[next]);
    prev[`${next}`] = env[next];
    return prev;
  }, {});
  await page.evaluate((envKeys) => {
    /**/
    window.uploadFile = function (relativePathName) {
      // Define the folder path for this example.
      var serverUrl = envKeys.DEV_SITE_URL;
      var serverRelativeUrlToFolder = envKeys.DEV_SITE_RELATIVE_URL + envKeys.DEV_SITE_SRC_RELATIVE_SERVER_MAP;

      // Get test values from the file input and text input page controls.
      var fileInput = $('#getFile');
      // var newName = $("#displayName").val();
      var newName = '';

      const test = '';
      // Get the server URL.


      // Initiate method calls using $ promises.
      // Get the local file as an array buffer.
      var getFile = getFileBuffer();
      getFile.done(function (arrayBuffer) {
        // Add the file to the SharePoint folder.
        var addFile = addFileToFolder(arrayBuffer, relativePathName);
        addFile.done(function (file, status, xhr) {
          // Get the list item that corresponds to the uploaded file.
          // debugger;
          var getItem = getListItem(file.d.ListItemAllFields.__deferred.uri);
          getItem.done(function (listItem, status, xhr) {
            // Change the display name and title of the list item.
            var changeItem = updateListItem(listItem.d.__metadata);
            changeItem.done(function (data, status, xhr) {
              console.log('file uploaded and updated');
            });
            changeItem.fail(onError);
          });
          getItem.fail(onError);
        });
        addFile.fail(onError);
      });
      getFile.fail(onError);

      // Get the local file as an array buffer.
      function getFileBuffer() {
        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
          deferred.resolve(e.target.result);
        };
        reader.onerror = function (e) {
          deferred.reject(e.target.error);
        };
        reader.readAsArrayBuffer(fileInput[0].files[0]);
        return deferred.promise();
      }

      // Add the file to the file collection in the Shared Documents folder.
      function addFileToFolder(arrayBuffer, relativePathName) {
        // debugger;
        // Get the file name from the file input control on the page.
        var parts = fileInput[0].value.split('\\');
        var fileName = parts[parts.length - 1];

        console.log(relativePathName);
        var browserPath = envKeys.PUPPETEER_WATCH_SRC_PATH.split('./').join('');
        browserPath = browserPath.split('./').join('');
        relativePathName = relativePathName.split(fileName).join('');
        relativePathName = relativePathName.split('\\').join('/');
        relativePathName = relativePathName.split(browserPath).join('');

        console.log(relativePathName);
        // var fileName = '/css/test.css';
        // relativePathName = "css";

        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
          "{0}/_api/web/getfolderbyserverrelativeurl('{1}')/files"
            + "/add(overwrite=true, url='{2}')",
          serverUrl,
          serverRelativeUrlToFolder + '/' + relativePathName,
          fileName,
        );
        console.log(fileCollectionEndpoint);

        // Send the request and return the response.
        // This call returns the SharePoint file.
        return $.ajax({
          url: fileCollectionEndpoint,
          type: 'POST',
          data: arrayBuffer,
          processData: false,
          headers: {
            accept: 'application/json;odata=verbose',
            'X-RequestDigest': $('#__REQUESTDIGEST').val(),
            // "content-length": arrayBuffer.byteLength
          },
        });
      }

      // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
      function getListItem(fileListItemUri) {
        // Send the request and return the response.
        return $.ajax({
          url: fileListItemUri,
          type: 'GET',
          headers: { accept: 'application/json;odata=verbose' },
        });
      }

      // Change the display name and title of the list item.
      function updateListItem(itemMetadata) {
        // Define the list item changes. Use the FileLeafRef property to change the display name.
        // For simplicity, also use the name as the title.
        // The example gets the list item type from the item's metadata, but you can also get it from the
        // ListItemEntityTypeFullName property of the list.
        var body = String.format(
          "{{'__metadata':{{'type':'{0}'}},'FileLeafRef':'{1}','Title':'{2}'}}",
          itemMetadata.type,
          newName,
          newName,
        );

        // Send the request and return the promise.
        // This call does not return response content from the server.
        return $.ajax({
          url: itemMetadata.uri,
          type: 'POST',
          data: body,
          headers: {
            'X-RequestDigest': $('#__REQUESTDIGEST').val(),
            'content-type': 'application/json;odata=verbose',
            // "content-length": body.length,
            'IF-MATCH': itemMetadata.etag,
            'X-HTTP-Method': 'MERGE',
          },
        });
      }
    };

    // Display error messages.
    window.onError = function (error) {
      console.log(error.responseText);
    };

    /**/
  }, env);

  const inputUploadHandle = await page.$('#getFile');

  watch(process.env.PUPPETEER_WATCH_SRC_PATH, { recursive: true }, async function (evt, name) {
    console.log('%s changed.', name);
    // console.log(name);
    // Sets the value of the file input to fileToUpload
    await inputUploadHandle.uploadFile(name);
    await page.evaluate((name) => {
      uploadFile(name);
    }, name);
  });
})();
